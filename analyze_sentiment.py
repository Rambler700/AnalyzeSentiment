# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 23:03:53 2018

@author: enovi
"""

import nltk

#Sentiment analysis demonstration
def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Article Text Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_sentiment(article_text)
             choice = True
         elif c == 'n':
             choice = False

def get_text():
    article_text = ''
    print('This program analyzes the sentiment of a message about the stock market.')
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_sentiment(article_text):
    positive_words = 0
    negative_words = 0
    
    positive_word_list = ['acquire', 'add', 'assure', 'bottom', 'bottom', 'bought', 'bounce',
                          'bright', 'bull', 'buy', 'call', 'cheer', 'climb', 'comeback',
                          'confident', 'escalate', 'expand', 'gain', 'get', 'good', 'great', 'hope',
                          'increase', 'long', 'obtain', 'optimism', 'pick', 'positive',
                          'procure', 'purchase', 'rally', 'react', 'rebound', 'recover', 'renew',
                          'resurge', 'revive', 'rose', 'sanguine', 'skyrocket', 'soar', 'succeed',
                          'up', 'upbeat', 'uptick', 'win']
    
    negative_word_list = ['abate', 'alter', 'bad', 'bear', 'corrupt', 'crash', 'decay', 'decline',
                          'decrease', 'depreciate', 'deteriorate', 'devalue', 'diminish',
                          'disappear', 'dispose', 'distort', 'dive', 'divest', 'down', 'downtick',
                          'downturn', 'drop', 'dwindle', 'ebb', 'exploit', 'fade', 'fail', 'fell',
                          'fiddle', 'fraud', 'gone', 'lessen', 'lose', 'manipulation', 'negative',
                          'obsolete', 'out', 'plunge', 'put', 'recede', 'rig', 'scam', 'scheme',
                          'sell', 'short', 'shrank', 'shrink', 'sink', 'slack', 'slump', 'sold',
                          'subside', 'tank', 'terrible', 'top', 'tumble', 'vanish', 'wane',
                          'waste', 'wither']
    
    article_text = article_text.lower().split()
    
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in positive_word_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in negative_word_list:
        word = nltk.PorterStemmer().stem(word)    
    
    for word in article_text:
        if word in positive_word_list:
            positive_words += 1
        elif word in negative_word_list:
            negative_words += 1
    
    #print results
    print('Positive sentiment was {}.'.format(positive_words))
    print('Negative sentiment was {}.'.format(negative_words))
    if positive_words > negative_words * 2:
        print('Overall sentiment was very positive.')
    elif positive_words > negative_words:
        print('Overall sentiment was positive.')
    elif negative_words > positive_words * 2:
        print('Overall sentiment was very negative.')
    elif negative_words > positive_words:
        print('Overall sentiment was negative.')
    else:
        print('Overall sentiment was neutral.')
#run program
main()
